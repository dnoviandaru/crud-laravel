@extends('layout')

@section('content')
<div class="row" style="padding:10px">
  <div class="col-lg-12">
    <div class="pull-left">
      <h2>Add New Article</h2>
    </div>
    <div class="pull-right">
      <a href="{{route('articles.index')}}" class="btn btn-primary">Back</a>
    </div>
  </div>
</div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

{!! Form::open(array('route'=>'articles.store','method'=>'post')) !!}
{{csrf_field()}}
  @include('article.form')
{!! Form::close() !!}
@endsection
