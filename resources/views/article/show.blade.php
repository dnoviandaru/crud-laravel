@extends('layout')

@section('content')
<div class="row" style="padding:10px">
  <div class="col-lg-12">
    <div class="pull-left">
      <h2>Show Article</h2>
    </div>
    <div class="pull-right">
      <a href="{{route('articles.index')}}" class="btn btn-primary">Back</a>
    </div>
  </div>
</div>

<div class="row" style="padding:10px">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Title :</strong>
      {{$article->title}}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Body :</strong>
      {{$article->body}}
    </div>
  </div>
</div>
@endsection
