@extends('layout')

@section('content')
<div class="row" style="padding:10px">
  <div class="col-lg-12">
    <div class="pull-left">
      <h2>Laravel CRUD</h2>
    </div>
    <div class="pull-right">
      <a href="{{route('articles.create')}}" class="btn btn-success">Create New Article</a>
    </div>
  </div>
</div>

@if($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{$message}}</p>
</div>
@endif

<table class="table table-bordered">
  <tr>
    <th width="50px">No</th>
    <th>Title</th>
    <th>Body</th>
    <th width="250px">Action</th>
  </tr>
  @foreach($articles as $article)
  <tr>
    <td>{{++$i}}</td>
    <td>{{$article->title}}</td>
    <td>{{$article->body}}</td>
    <td>
      <a class="btn btn-info" href="{{route('articles.show',$article->id)}}">Show</a>
      <a class="btn btn-primary" href="{{route('articles.edit', $article->id)}}">Edit</a>
      {!! Form::open(['method' => 'DELETE', 'route' =>['articles.destroy',$article->id],'style'=>'display:inline'])!!}
      {{csrf_field()}}
      {!! Form::submit('Delete', ['class'=>'btn btn-danger'])!!}
      {!! Form::close() !!}
    </td>
  </tr>
  @endforeach
</table>

  {!! $articles->links() !!}
@endsection
