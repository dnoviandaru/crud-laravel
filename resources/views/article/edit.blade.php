@extends('layout')

@section('content')
<div class="row" style="padding:10px">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>Edit Article</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('articles.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($articles, ['method' => 'PATCH','route' => ['articles.update', $articles->id]]) !!}
    {{csrf_field()}}
        @include('article.form')
    {!! Form::close() !!}

@endsection
